// 📕 Constants ---------------------------------

const CLIENT_ID = '361640580028-8uuhkid2hdpq6m5lhmavdunp79k98gjk.apps.googleusercontent.com';
const API_KEY = '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';

// Discovery doc URL for APIs used by the quickstart
const DISCOVERY_DOC = 'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest';

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
let scopes = [
  // 'https://www.googleapis.com/auth/gmail.readonly',
  // 'https://www.googleapis.com/auth/gmail.labels',
  // 'https://www.googleapis.com/auth/gmail.send',
  // 'https://mail.google.com/',
  // 'https://www.googleapis.com/auth/gmail.addons.current.action.compose',
  // 'https://www.googleapis.com/auth/gmail.compose',
  'https://www.googleapis.com/auth/gmail.modify',
];
const SCOPES = scopes.join(' ');

const EMAIL_LABEL = 'my-receipts';

let tokenClient;

// inital values for page view
// state uses local storage to persist across tabs and reloads
// url is used to hold individual page view state
const initialPageViewState = {
  error: '',
  gapiStatus: 'loading',
  gisStatus: 'loading',
  loaderStatus: 'hidden',
  sheet: '',
  debug: true,
  search: null,
  pagination: {
    page: 0,
    size: 5
  },
};

const STATE_KEY = 'receipts_state';

// relative to baseUri
const VIEWS = new Map();

VIEWS.set('receipts', {
  template: ''
});

VIEWS.set('emails', {
  template: 'emails'
});

VIEWS.set('auth', {
  template: 'auth'
});

const urlDelimiterPattern = /\?|\/|&|=/g;

let queue = [];

// ⚙️ Util ---------------------------------

function getState(){
  let state;
  try{
    state = JSON.parse(localStorage.getItem(STATE_KEY))
  }catch(e){}
  return state || {};
}

function setState(state){
  try{
    localStorage.setItem(STATE_KEY, JSON.stringify(state))
  }catch(e){}
}

function dispatch(action, ...rest){
  if(getState().debug){
    console.log(`dispatching "${action}"`, rest);
  }
  actions[action].forEach(act => act(...rest));
}

// function pushState(url){
//   history.pushState({}, null, url);

//   dispatch(
//     'view',
//     mapUrl(
//       relativeUrl(),
//       VIEWS
//     )
//   );
// }

// 🎬 Actions ---------------------------------

const actions = {
  label: [(labelId) => setState({...getState(), labelId})],
  loader: [(loaderStatus) => setState({...getState(), loaderStatus}), loaderAction],
  receipts: [(receipts) => setState({...getState(), receipts}), resetPaginationAction],
  search: [(search) => setState({...getState(), search}), resetPaginationAction],
  pagination: [(pagination) => setState({...getState(), pagination})],
  view: [(view) => setState({...getState(), view}), viewAction, resetSheetAction, resetCanvasAction],
  sheet:[(sheet) => setState({...getState(), sheet}), sheetAction],
  error: [(error) => setState({...getState(), error}), resetTokenAction],
  gapi: [(gapiStatus) => setState({...getState(), gapiStatus}), gapiStatusAction, fetchDataAction],
  gis: [(gisStatus) => setState({...getState(), gisStatus}), fetchDataAction],
  token: [(token) => setState({...getState(), token}), tokenAction, fetchDataAction]
};

function resetCanvasAction(){
  const canvas = document.querySelector('canvas');
  const context = canvas.getContext('2d');
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function resetTokenAction(){
  if(getState().error){
    dispatch('token', '');
  }
}

function loaderAction(loaderStatus){
  Array.from(document.querySelectorAll('.ispinner'))
  .forEach(loader => {
    loader.setAttribute('data-visible', loaderStatus);
  })
}

function resetSheetAction(){
  dispatch('sheet', '')
}

function resetPaginationAction(){
  dispatch('pagination', initialPageViewState.pagination, true)
}

function sheetAction(sheet){
  if(!sheet){
    document.documentElement.removeAttribute('data-sheet-active');
    return;
  }
  document.documentElement.setAttribute(
    'data-sheet-active',
    sheet
  );
}

// view object is {name, values}
async function viewAction({name}, refetch){
  document.documentElement.setAttribute(
    'data-view-active',
    name
  );

  renderView(getState().view, refetch)
}

function tokenAction(token){
  let {gapiStatus} = getState();
  if(gapiStatus === 'inited'){
    gapi.client.setToken(token);
  }
}

function gapiStatusAction(status){
  if(status === 'loaded'){
    gapi.load('client', initializeGapiClient);
  }
}

function queueAction(fn){
  queue.push(fn)
  emptyQueue()
}

function emptyQueue(){
  let {gapiStatus, gisStatus} = getState();
  if (gapiStatus === 'inited' && gisStatus === 'loaded') {
    while(queue.length){
      queue.shift()()
    }
  }
}

async function fetchDataAction(){
  let {gapiStatus, gisStatus, token, view} = getState();

  if (gapiStatus === 'inited' && gisStatus === 'loaded') {

    // don't block
    getLabelId()
      .then((labelId) => {
        if(labelId){
          dispatch('label', labelId);
        }
      })



    if(token){
      gapi.client.setToken(token);
      dispatch('sheet', '')
      if(view.name === 'receipts'){
        renderReceipts(true); // re-render receipts page (fetches messages)
      }
      if(view.name === 'emails'){
        renderSearchPage(true); // re-render search page (fetches messages)
      }
    }else{
      dispatch('sheet', 'auth')
    }
  }
}

// Takes relative url segment and maps to a view config object and values
function mapUrl(url, views){
  const slugs = url.split(urlDelimiterPattern);
  let values, view;

  for (const [name, config] of views) {
    values = {};

    const match = !config.template
      .split(urlDelimiterPattern)
      .some((slug, i) => {
        if(slug.indexOf(':') === 0){
          values[slug.replace(':','')] = slugs[i];
          return false;
        }
        if(slugs[i] === slug){
          return false;
        }
        return true; // no match
      })

    if(match){
      view = name;
      break;
    }
  }

  return {
    name: view,
    values,
  };
}

// Takes a view configuration object and values and returns a relative url segment string
function serializeView(view, values = {}){
  const {template} = view;
  let cursor = 0; // index of last delimiter

  return template.split(urlDelimiterPattern)
    .map((key, i) => {
      let lastDelimiter = i > 0 ? view[cursor] : '';
      cursor += key.length + (i > 0 ? 1 : 0);
      if(key.indexOf(':') === 0){
        return lastDelimiter + (values[key.replace(':','')] || '');
      }
      return lastDelimiter + key;
    })
    .join('');
}

// Takes a relative url segment string and resolves relative to base uri
function fullUrl(location){
  const a = document.createElement('a');
  a.href = location;
  return a.href;
}

// Takes a full url and returns url segment relative to base uri
function relativeUrl(url){
  url = url || window.location.toString();
  return url.replace(document.baseURI, '');
}

// 🦫 Services ---------------------------------

// Callback after api.js is loaded.
function gapiLoaded(){
  dispatch('gapi', 'loaded');
}

// Callback after Google Identity Services are loaded
function gisLoaded(){
  tokenClient = google.accounts.oauth2.initTokenClient({
    client_id: CLIENT_ID,
    scope: SCOPES,
    callback: '', // defined later
  });
  dispatch('gis', 'loaded');
}

/**
 * Callback after the API client is loaded. Loads the
 * discovery doc to initialize the API.
 */
async function initializeGapiClient() {
  await gapi.client.init({
    apiKey: API_KEY,
    discoveryDocs: [DISCOVERY_DOC],
  });
  dispatch('gapi', 'inited');
}

// 🧑‍🎨️ Render  ---------------------------------

function renderView(view, refetch){
  switch(view.name){
    case 'receipts':
      renderReceipts(refetch);
      break;
    case 'emails':
      renderSearchPage(refetch);
      break;
    case 'file':
      renderFilePage();
      break;
  }
}

async function renderFilePage(){
  const content = document.querySelector(`[data-view="receipts"] .content`);

  let canvas = document.querySelector('canvas');
	let context = canvas.getContext("2d");
	let fileinput = document.querySelector('input'); // input file
	let img = new Image();

  let files = fileinput.files; // FileList object
	let file = files[0];

  if(file.type.toUpperCase().includes('HEI')){
    dispatch('loader', 'visible');
        // get image as blob url
        let blobURL = URL.createObjectURL(file);

        // convert "fetch" the new blob url
        let blobRes = await fetch(blobURL)

        // convert response to blob
        let blob = await blobRes.blob()

        // convert to PNG - response is blob
        let conversionResult = await heic2any({ blob ,toType: "image/png",})

        // convert to blob url
        var url = URL.createObjectURL(conversionResult);
        var i = new Image();

        let res;
        let p = new Promise((r)=>{res = r});
        i.onload = ()=>{

          const scale = 2016 / i.width;
          canvas.width = i.width * scale;
          canvas.height = i.height * scale;

          // canvas.width = i.width;
          // canvas.height = i.height;

          context.imageSmoothingQuality = "high";
          // context.drawImage(i, 0, 0, i.width, i.height);
          context.drawImage(i, 0, 0, canvas.width, canvas.height);
          res()
        }

        i.src = url;
        await p;
      dispatch('loader', 'hidden');
    } else{
       let reader = new FileReader();
    // Read in the image file as a data URL.
    reader.readAsDataURL(file);
    reader.onload = function(evt){
      if( evt.target.readyState == FileReader.DONE) {
        img.onload = () => {
          const scale = 2016 / img.width;
          canvas.width = img.width * scale;
          canvas.height = img.height * scale;

          // canvas.width = img.width;
          // canvas.height = img.height;

          context.imageSmoothingQuality = "high";
          // context.drawImage(img, 0, 0, img.width, img.height);
          context.drawImage(img, 0, 0, canvas.width, canvas.height);

          canvas.setAttribute('data-type', file.type)
        };
        img.src = evt.target.result;
      }
    }
    }
}

function renderPagination(total){
  const {pagination} = getState();
  const start = pagination.page * pagination.size;
  const end = start + pagination.size;

  const showNext = end < total;
  const showPrevious = start > 0;
  const pag =  document.createElement('div');
  pag.classList.add('pagination');

  if(showPrevious){
    const prev = document.createElement('button')
    prev.classList.add('button-scroll', 'prev')
    prev.textContent = 'Previous';
    const pg =  document.createElement('div');
    pg.textContent = `Page ${pagination.page + 1}`;
    pag.appendChild(prev)
    pag.appendChild(pg);

  }
  if(showNext){
    const next = document.createElement('button')
    next.classList.add('button-scroll', 'next')
    next.textContent = 'Next';
    pag.appendChild(next);
  }
  return pag;
}

function renderEmail(email){
  const fragment = document.createDocumentFragment();
  const card = document.createElement('div')
  card.classList.add('card')
  const face = document.createElement('div');
  face.classList.add('face')
  card.appendChild(face)

  const footer = document.createElement('div')
  footer.classList.add('footer')

  const addbtn = document.createElement('button')
  addbtn.classList.add('button', 'add-email')
  addbtn.textContent = email.labelIds.includes(getState().labelId) ? 'Remove' : 'Add';
  addbtn.setAttribute('data-email', email.id)

  footer.appendChild(addbtn)

  console.log(email)

  face.appendChild(renderCloseIcon());


  if(email.payload.headers.find(({name, value}) => (name === 'Subject' && value === 'My Receipt'))){
    const attachmentId = email.payload.parts.find(({mimeType})=>(mimeType === 'image/png')).body.attachmentId;
    if(!attachmentId){
      console.log('missed attachment for', email)

    }else{
      // make this non blocking and wait for gapi to be ready

      face.classList.add('face-image');
      const act = () => {
        getAttachment(email.id, attachmentId)
          .then(attachment => {
            face.appendChild(renderImage(attachment));
            face.appendChild(footer)
          })
      }
      queueAction(act)
    }

  }else{
    let data = email.payload.body.size > 0 ?
      email.payload?.body?.data :
      email.payload?.parts?.find(({mimeType})=>(mimeType === 'text/html'))?.body?.data;
    if(!data){
      return;
    }
    face.appendChild(renderIframe(data));
    face.appendChild(footer)
  }

  // if(attachment){
  //   face.appendChild(renderImage(attachment))
  //   face.classList.add('face-image');
  // }else{
  //   let data = email.payload.body.size > 0 ?
  //     email.payload?.body?.data :
  //     email.payload?.parts?.find(({mimeType})=>(mimeType === 'text/html'))?.body?.data;
  //   if(!data){
  //     return;
  //   }
  //   face.appendChild(renderIframe(data));
  // }


  fragment.appendChild(card);

  return fragment;
}

function renderImage(data){
  const image = document.createElement('img');
  image.classList.add('img');
  image.src = 'data:image/png;base64,' + data.replace(/\-/g, '+').replace(/\_/g, '/');
  return image;
}

async function renderReceipts(refetch){
  const {pagination, token, error} = getState();
  const start = pagination.page * pagination.size;
  const end = start + pagination.size;

  const content = document.querySelector(`[data-view="receipts"] .content`);
  content.innerHTML = '';

  dispatch('loader', 'visible');
  let emails;

  if(refetch){
    emails = await getReceipts();
    if(emails && emails.length || (token && !error)){
      dispatch('receipts', emails)
    }else{
      emails = getState().receipts;
    }
  }else{
    emails = getState().receipts;
  }

  emails?.slice(start, end).forEach( email => {
    const r = renderEmail(email);
    if(r){
      content.appendChild(r);
    }
  })

  content.appendChild(renderPagination(emails?.length || 0))
  document.body.scrollTop = 0;
  bindFace();
  bindClose();
  bindAddEmail();
  bindPrevious();
  bindNext();
  dispatch('loader', 'hidden');
}

async function renderSearchPage(refetch){
  const {pagination} = getState();
  const start = pagination.page * pagination.size;
  const end = start + pagination.size;

  const content = document.querySelector(`[data-view="emails"] .content`);
  content.innerHTML = '';

  dispatch('loader', 'visible');
  let messages, emails;

  if(refetch){
    messages = await searchMessages();
    dispatch('search', messages)
  }else{
    messages = getState().search;
  }

  if(messages && messages.length){
    emails = await Promise.all(messages.slice(start, end).map(({id})=>{
      return getMessage(id);
    }));
  }

  emails?.forEach( (email) => {
    const r =  renderEmail(email);
    if(r){
      content.appendChild(r);
    }
  })

  content.appendChild(renderPagination(messages?.length || 0))
  document.body.scrollTop = 0;
  bindFace();
  bindClose();
  bindAddEmail();
  bindPrevious();
  bindNext();
  dispatch('loader', 'hidden');
}

function renderIframe(data){
  data = atob(data.replace(/\-/g, '+').replace(/\_/g, '/'));
  const iframe = document.createElement('iframe');
  const blob = new Blob([data], {type: 'text/html'});
  // iframe.setAttribute('scrolling', 'no');
  iframe.setAttribute('src', window.URL.createObjectURL(blob));
  return iframe;
}

function renderCloseIcon(){
  const template = document.querySelector("#closeIconTemplate");
  return template.content.cloneNode(true);
}

// 🏃‍♀️ Runtime  ---------------------------------

setState({...getState(), ...initialPageViewState})
dispatch('view', {name: 'receipts'})

// 👂 Event Listeners  ---------------------------------

document.querySelector('input')
  .addEventListener('change', ()=>{
    dispatch('view', {name: 'file'})
  })

Array.from(document.querySelectorAll('[data-sheet]'))
  .forEach(button => {
    button.addEventListener('click', (evt)=>{
      evt.stopPropagation();
    })
  })

Array.from(document.querySelectorAll('.add'))
  .forEach(button => {
    button.addEventListener('click', (evt) => {
      evt.stopPropagation();
      dispatch('sheet', 'add')
    })
  })

  Array.from(document.querySelectorAll('.auth'))
  .forEach(button => {
    button.addEventListener('click', (evt) => {
      evt.stopPropagation();
      dispatch('sheet', 'auth')
    })
  })

function bindFace(){
  Array.from(document.querySelectorAll('.face'))
    .forEach(face => {
      face.addEventListener('click', (evt) => {
        face.classList.add('focus');
        const iframe = face.querySelector('iframe');
        if(iframe){
          iframe.removeAttribute('scrolling')
        }
      })
    })
}
bindFace()

function bindClose(){
  Array.from(document.querySelectorAll('.close'))
    .forEach(btn => {
      btn.addEventListener('click', (evt) => {
        evt.stopPropagation();
        btn.parentNode.classList.remove('focus');
        const iframe = btn.parentNode.querySelector('iframe');
        if(iframe){
          iframe.setAttribute('scrolling', 'no')
        }
      })
    })
}
bindClose()

function bindPrevious(){
  Array.from(document.querySelectorAll('.prev'))
    .forEach(btn => {
      btn.addEventListener('click', (evt) => {
        const cur = getState().pagination;
        dispatch('pagination', {...cur, page: cur.page-1})
        renderView(getState().view, false)
      })
    })
}
bindPrevious()

function bindNext(){
  Array.from(document.querySelectorAll('.next'))
    .forEach(btn => {
      btn.addEventListener('click', (evt) => {
        const cur = getState().pagination;
        dispatch('pagination', {...cur, page: cur.page+1})
        renderView(getState().view, false)
      })
    })
}
bindNext()

function bindAddEmail(){
  Array.from(document.querySelectorAll('.add-email'))
    .forEach(btn => {
      btn.addEventListener('click', async (evt) => {
        evt.stopPropagation();
        btn.parentNode.classList.remove('focus');
        const iframe = btn.parentNode.querySelector('iframe');
        if(iframe){
          iframe.setAttribute('scrolling', 'no')
        }

        await labelEmail(btn.getAttribute('data-email'), getState().labelId, btn.textContent === 'Remove')

        const {view} = getState();
        if(view.name === 'receipts' && btn.textContent === 'Remove'){
          renderReceipts(true); // re-render receipts page (fetches messages)
        }

        btn.textContent = btn.textContent === 'Remove' ? 'Add' : 'Remove';

      })
    })
}
bindAddEmail()

Array.from(document.querySelectorAll('.dismiss'))
  .forEach(btn => {
    btn.addEventListener('click', (evt) => {
      evt.stopPropagation();
      dispatch('sheet', '');
    })
  })

Array.from(document.querySelectorAll('.done'))
  .forEach(btn => {
    btn.addEventListener('click', (evt) => {
      dispatch('view', {name: 'receipts'}, true);
    })
  })

Array.from(document.querySelectorAll('.email'))
  .forEach(btn => {
    btn.addEventListener('click', (evt) => {
      evt.stopPropagation();
      dispatch('view', {name: 'emails'}, true);
    })
  })

Array.from(document.querySelectorAll('.authorize'))
  .forEach(btn => {
    btn.addEventListener('click', (evt) => {
      // assign callback
      tokenClient.callback = async (resp) => {
        if (resp.error !== undefined) {
          throw (resp);
        }

        if (resp) {
          dispatch('token', resp);
        }
      };

      // request token
      if (gapi.client.getToken() === null) {
        // Prompt the user to select a Google Account and ask for consent to share their data
        // when establishing a new session.
        tokenClient.requestAccessToken({prompt: 'consent'});
      } else {
        // Skip display of account chooser and consent dialog for an existing session.
        tokenClient.requestAccessToken({prompt: ''});
      }
    })
  })

document.body.addEventListener('click', () => {
    if(getState().sheet){
      dispatch('sheet', '');
    }
})

Array.from(document.querySelectorAll('.send'))
  .forEach(btn => {
    btn.addEventListener('click', async (evt) => {
      evt.stopPropagation();
      await sendMessage();
      dispatch('view', {name: 'receipts'}, true);
    })
  })

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick() {
  const token = gapi.client.getToken();

  if (token !== null) {
    google.accounts.oauth2.revoke(token.access_token);
    // gapi.client.setToken('');

    dispatch('token', '');
    dispatch('error', '');
  }
}

// 🌐 Network  ---------------------------------

async function getAttachment(messageId, attachmentId){
  let response;
  try{
    response = await gapi.client.gmail.users.messages.attachments.get({
      'userId': 'me',
      'messageId': messageId,
      'id': attachmentId
    });
  }catch (err){
    dispatch('error', err);
    return;
  }
  return response?.result?.data;
}

async function getEmail(){
  let response;
  try{
    response = await gapi.client.gmail.users.getProfile({
      'userId': 'me',
    });
  }catch (err){
    dispatch('error', err);
    return;
  }
  return response?.result?.emailAddress;
}

async function sendMessage(){
  dispatch('loader', 'visible')
  const canvas = document.querySelector('canvas');

  // const resize = document.createElement('canvas');
  // const context = resize.getContext('2d');
  // const scale = canvas.width / 2016;

  // resize.width = canvas.width * scale;
  // resize.height = canvas.height * scale;

  // context.imageSmoothingQuality = "high";
  // context.drawImage(canvas, 0, 0, resize.width, resize.height);

  // let dataUrl = resize.toDataURL();

  let dataUrl = canvas.toDataURL();
  let attachmentData = dataUrl.split('base64,')[1];

  const boundary = 'my_boundary';

  // Create the attachment part
  const attachmentPart = [
    '--' + boundary,
    'Content-Type: image/png',
    'Content-Disposition: attachment; filename="image.png"',
    'Content-Transfer-Encoding: base64',
    '',
    attachmentData,
    '',
  ].join('\n');

  const emailAddress = await getEmail();

  const message = [
    'Content-Type: multipart/mixed; boundary="' + boundary + '"',
    'To: ' + emailAddress,
    'Subject: ' + 'My Receipt',
    '',
    '--' + boundary,
    '',
    attachmentPart,
    '--' + boundary + '--',
  ].join('\n');

  let response;
  try{
    response = await gapi.client.gmail.users.messages.send({
      'userId': 'me',
      'resource': {
        'raw': btoa(message)
      }
    });
  }catch (err){
    // dispatch('error', err)
    console.log(err)
    return;
  }
  const result = response.result;
  if (!result) {
    return;
  }
  await labelEmail(result.id, getState().labelId)
  dispatch('loader', 'hidden')
  return result;
}

// gets one message
async function getMessage(id) {
  let response;
  try{
    response = await gapi.client.gmail.users.messages.get({
      'userId': 'me',
      'id': id,
      'format': 'full'
    });
  }catch (err){
    dispatch('error', err)
    return;
  }
  const message = response.result;
  if (!message) {
    return;
  }
  return message;
}

// searches unlabeled messages
async function searchMessages(){
  let response;
  try{
    response = await gapi.client.gmail.users.messages.list({
      'userId': 'me',
      'q': `receipt -label:${EMAIL_LABEL}`,
    });
  }catch (err){
    dispatch('error', err);
    return;
  }
  const messages = response.result.messages;
  if (!messages || messages.length == 0) {
    // dispatch('error', 'No messages found');
    return;
  }

  return messages;
}

// gets all messages labled
async function getReceipts(){
  let response;
  try{
    response = await gapi.client.gmail.users.messages.list({
      'userId': 'me',
      'q': `label:${EMAIL_LABEL}`,
    });
  }catch (err){
    dispatch('error', err);
    return;
  }
  const messages = response.result.messages;
  if (!messages || messages.length == 0) {
    // dispatch('error', 'No messages found');
    return;
  }

  const expanded = await Promise.all(messages.map(({id}) => getMessage(id)))

  const attachments = Promise.all(expanded.map(async (email) => {
    // if(email.payload.headers.find(({name, value}) => (name === 'Subject' && value === 'My Receipt'))){
    //   const attachmentId = email.payload.parts.find(({mimeType})=>(mimeType === 'image/png')).body.attachmentId;
    //   if(!attachmentId){
    //     console.log('missed attachment for', email)
    //     return email;
    //   }
    //   const attachment = await getAttachment(email.id, attachmentId);
    //   return {...email, attachment};
    // }
    return email;
  }))

  return attachments;
}

async function getLabelId(){
  let response;
  try{
    response = await gapi.client.gmail.users.labels.list({
      'userId': 'me',
    });
  }catch (err){
    // dispatch('error', err); dispatching error clears token and opens auth popup
    return;
  }
  const labels = response.result.labels;

  if (!labels || labels.length == 0) {
    return;
  }

  const fullLabels = await Promise.all(labels.map(l => getLabel(l.id)));

  const existingLabel = fullLabels.find(l => l.name === EMAIL_LABEL) || '';

  if(existingLabel){
    return existingLabel.id;
  }

  const l = await createLabel();

  return l?.id || '';
}

async function getLabel(id){
  let response;
  try{
    response = await gapi.client.gmail.users.labels.get({
      'userId': 'me',
      'id': id
    });
  }catch (err){
    dispatch('error', err)
    return;
  }
  const label = response.result;
  if (!label) {
    return;
  }
  return label;
}

async function createLabel(){
  let response;
  try{
    response = await gapi.client.gmail.users.labels.create({
      'userId': 'me',
      'labelListVisibility': 'labelShow',
      'messageListVisibility': 'show',
      'name': EMAIL_LABEL
    });
  }catch (err){
    dispatch('error', err)
    return;
  }
  const label = response.result;
  if (!label) {
    return;
  }
  return label;
}

async function labelEmail(messageId, labelId, remove = false){
  let response;
  const payload = {};
  payload[remove ? 'removeLabelIds' : 'addLabelIds'] = [labelId];
  try{
    response = await gapi.client.gmail.users.messages.modify({
      'userId': 'me',
      'id': messageId,
      ...payload
    });
  }catch (err){
    dispatch('error', err)
    return;
  }
  const result = response.result;
  if (!result) {
    return;
  }
  return result;
}
