# Dev notes

## Testing locally
1. http-server is installed as global package
2. run with cache disabled -c-1

## Setup google project

1. create project "Receipts"
2. enable gmail API
3. configure oauth screen
  - external
  - skip scopes
  - add users
4. make client id for app
  - create client id "Receipts Web Client"
  - setup gitlab page for authorized js origin URI "https://bsawyer.gitlab.io" and "http://localhost:9000"
  Client ID: 361640580028-8uuhkid2hdpq6m5lhmavdunp79k98gjk.apps.googleusercontent.com
  Client secret: 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
5. create API key
  - defaults to unrestricted
  API key: AIzaSyAblJRD6p_oCom-5e2QLx1jevoKh0-MdJw
6. copy code from quickstart
  - Quickstart url: https://developers.google.com/gmail/api/quickstart/js
  - run http-server
7. reference docs
  Gmail API docs: https://developers.google.com/gmail/api/reference/rest
  Google API client lib repository: https://github.com/google/google-api-javascript-client

## Listing messages

1. calling the messages.list endpoint returns only the threadID
2. fetch addition message details with messages.get
3. query format for searching messages https://support.google.com/mail/answer/7190?hl=en
4. consider options
  - using labels to track added receipts to client
      pros:
        keeps state in email, persists across clients and purges
        logic works with other email providers?
      cons:
        adds permission scope
        more requests to make
  - using email message id header
      pros:
        no added permission scope
        no extra requests to add labels
        logic works with other email providers
      cons:
        lose saved state across clients and client purges

## Auth

1. look into persisting client token
  Google Identity docs: https://developers.google.com/identity/oauth2/web/reference/js-reference
  - access token is short lived
  - refresh tokens are long lived and can be used to get new access tokens without user consent but require server

## Storage

1. added local storage and state actions
2. local storage is tied to host domain
  - all state for gitlab is stored under https://bsawyer.gitlab.io/ across projects
  - need to use project specific state name

## Page and state

1. looking into history api
2. differentiate between state across tabs, a single tab across reloads, and a single view
3. gitlab page redirect configuration copy index to 404 page
  - add 404.html to .gitignore
  - symbolic link index to 404. has to be absolute path
    ln -s /Users/bs031n/workspace/gitlab.com/bsawyer/receipts/public/index.html /Users/bs031n/workspace/gitlab.com/bsawyer/receipts/public/404.html
4. local development still uses http-server e.g. http-server -p 9000
  - problem resolve relative assets
  - gitlab path is host/<project-name>/
  - localhost is host/
  - instead of modifying html file in CI
    1. make a folder the same name as gitlab project name and put in public
    2. copy contents to public in CI
    3. html has relative paths to root with project name for assets
  - as an alternative to having the assets in a sub dir
    1. add empty base href to index.html
    2. add CI step to use sed to replace with gitlab url
5. using history api and the pageview and popstate events to keep track of view state
  - how to handle multiple tabs?
  - interesting note, if the app is updated and a user navigates pushed state, they will not fetch latest code
  - to counter this could be possible to fetch assets if view(push === false)

## Routing

1. routing functions with mapping and serializing templated urls

## UI

1. working on receipts view, add view and single receipt view
  - adding markup
  - adding styles
  - adding icons
    COCO icon pack V2: https://www.figma.com/file/78oMZXKUtFFNzb8m33aixD/COCO-V2---200-Icons-%F0%9F%94%A5-(Community)?type=design&node-id=1-1479&t=03uCYVHOh5xwOCEO-0

## Page load lifecycle

1. looked into the following events and decided to remove this logic
  - pageshow
  - visibility change
  - popstate

## Adding and removing labels requires modify permission

1. removed
  'https://www.googleapis.com/auth/gmail.readonly',
  'https://www.googleapis.com/auth/gmail.labels',
  'https://www.googleapis.com/auth/gmail.send',

## Images
1. heif and heic https://stackoverflow.com/questions/57127365/make-html5-filereader-working-with-heic-files
